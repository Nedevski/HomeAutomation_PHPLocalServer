<?php
require_once '../DAL/DbConfig.php';

if (!$user->is_loggedin()) {
    $user->redirect('login.php');
} else if (isset($_POST["generate_key"])) {
    $hash = $user->generate_new_key($_SESSION["user_id"]);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="style.css" type="text/css"/>
    <title>RPi System - <?= $_SESSION['user_name']; ?></title>
</head>
1
<body>

<div class="header">
    <div class="left">
        <label><a href="#">Raspberry Pi - Auth system -</a></label>
    </div>
    <div class="right">
        <label><a href="logout.php?logout=true"><i class="glyphicon glyphicon-log-out"></i> logout</a></label>
    </div>
</div>
<div class="content">
    Welcome : <?= $_SESSION['user_name']; ?>
    <br/>
    Role is <?= $_SESSION['user_role'] > 0 ? "Admin" : "User" ?>
    <br/>
    <?= (isset($hash) && $hash) ? "Key changed." . "<br/>" : "" ?>
    You key is <?= $user->getKey($_SESSION["user_id"]) ?>
    <form method="post">
        <input type="submit" value="Generate new key" name="generate_key">
    </form>
</body>
</html>