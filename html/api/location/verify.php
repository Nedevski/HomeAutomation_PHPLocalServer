<?php
require_once '../../../DAL/DbConfig.php';
require_once '../../../DAL/Helpers.php';

if (!isset($_POST)) {
    returnJsonMessage(array(
        "Message" => "Missing POST data"
    ), 400);
} else {
    $userService = new User($DB_con);
    if (!isset($_SERVER['HTTP_KEY'])) {
        returnJsonMessage(array(
            "Message" => "Missing auth key"
        ), 401);
    } else if (!$userService->check_for_admin_key($_SERVER['HTTP_KEY'])) {
        returnJsonMessage(array(
            "Message" => "Invalid key"
        ), 401);
    } else {
        $request = json_decode(file_get_contents('php://input'), true);

        if (!isset($request['userId'])) {
            returnJsonMessage(array(
                "Message" => "Missing user ID"
            ), 400);
        }

        $remoteUserId = $request['userId'];
        $accessKey = $_SERVER['HTTP_KEY'];
        $userGuid = getGUID();

        $userService = new User($DB_con);

        $success = $userService->bind_external_to_admin($remoteUserId, $accessKey, $userGuid);

        if ($success) {
            returnJsonMessage($success, 200);
        } else {
            returnJsonMessage(array(
                "Message" => "Failed to register user"
            ), 400);
        }
    }
}