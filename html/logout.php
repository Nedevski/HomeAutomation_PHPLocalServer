<?php
	require_once '../DAL/DbConfig.php';
	require_once('../DAL/User.php');

	$user_logout = new User();
	
	if($user_logout->is_loggedin()!="")
	{
		$user_logout->redirect('index.php');
	}
	if(isset($_GET['logout']) && $_GET['logout']=="true")
	{
		$user_logout->logout();
		$user_logout->redirect('index.php');
	}
