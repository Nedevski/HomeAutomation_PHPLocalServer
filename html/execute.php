<?php
require_once '../DAL/DbConfig.php';

function returnBadRequest($message)
{
    header('Content-type: application/json');
    header('Bad request', true, 400);

    $json = json_encode($message);
    echo $json;
}

function returnOkJsonResponse($response)
{
    header('Content-type: application/json');
    header('OK', true, 200);

    echo $response;
}

$messages = array(
    'empty_post' => array(
        'errorMsg' => "No POST data found"
    ),
    'missing_key' => array(
        'errorMsg' => "Auth key or URL not found in the request"
    ),
    'invalid_key' => array(
        'errorMsg' => "Auth key is invalid"
    ),
    'error_on_device' => array(
        'errorMsg' => "Device communication failure"
    )
);

if (empty($_POST)) {
    returnBadRequest($messages["empty_post"]);
} else if (!isset($_POST['authKey']) || !isset($_POST['url'])) {
    returnBadRequest($messages["missing_key"]);
} else {
    $valid = $user->check_for_key($_POST['authKey']);

    if ($valid > 0) {
        $response = file_get_contents($_POST['url']);
        if (!$response) {
            returnBadRequest($messages["error_on_device"]);
        } else {
            returnOkJsonResponse($response);
        }
    } else {
        returnBadRequest($messages["invalid_key"]);
    }
}