<?php
require_once("DbConfig.php");

class User
{
    private $db;

    function __construct($DB_con)
    {
        $this->db = $DB_con;
    }

    public function register($uname, $upass)
    {
        try {
            $new_password = password_hash($upass, PASSWORD_DEFAULT);

            $stmt = $this->db->prepare("INSERT INTO users(user_name,user_pass)
                                                       VALUES(:uname, :upass)");

            $stmt->bindparam(":uname", $uname);
            $stmt->bindparam(":upass", $new_password);
            $stmt->execute();

            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function login($uname, $upass)
    {
        try {
            $stmt = $this->db->prepare("SELECT * FROM users WHERE user_name=:uname LIMIT 1");
            $stmt->execute(array(':uname' => $uname));
            $userRow = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($stmt->rowCount() > 0) {
                if (password_verify($upass, $userRow['user_pass'])) {
                    $_SESSION['user_id'] = $userRow['user_id'];
                    $_SESSION['user_name'] = $userRow['user_name'];
                    $_SESSION['user_role'] = $userRow['user_role'];
                    return true;
                } else {
                    return false;
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getRole($user_id)
    {
        try {
            $stmt = $this->db->prepare("SELECT * FROM users WHERE user_id=:uid LIMIT 1");
            $stmt->execute(array(':uid' => $user_id));
            $userRow = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($stmt->rowCount() > 0) {
                return $userRow['user_role'];
            } else return -1;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getKey($user_id)
    {
        try {
            $stmt = $this->db->prepare("SELECT * FROM users WHERE user_id=:uid LIMIT 1");
            $stmt->execute(array(':uid' => $user_id));
            $userRow = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($stmt->rowCount() > 0) {
                return $userRow['user_key'];
            } else return -1;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function is_loggedin()
    {
        if (isset($_SESSION['user_id'])) {
            return true;
        } else return false;
    }

    public function generate_new_key($user_id)
    {
        $hash = $this->generateNewHash();

        if (!isset($user_id)) {
            return false;
        }
        try {
            $stmt = $this->db->prepare("UPDATE users SET user_key = :ukey WHERE user_id = :uid");
            $success = $stmt->execute(array(':uid' => $user_id, ':ukey' => $hash));
            if ($success) {
                return $hash;
            } else return false;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function redirect($url)
    {
        header("Location: $url");
    }

    public function logout()
    {
        session_destroy();
        unset($_SESSION['user_session']);
        return true;
    }

    public function check_for_key($user_key)
    {
        if (!isset($user_key)) {
            return false;
        }
        try {
            $stmt = $this->db->prepare("SELECT * FROM users WHERE user_key = :ukey LIMIT 1");
            $success = $stmt->execute(array(':ukey' => $user_key));

            if ($success) {
                $userRow = $stmt->fetch(PDO::FETCH_ASSOC);

                if ($stmt->rowCount() == 1) {
                    return $userRow['user_id'];
                } else return -1;
            }

            return false;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function register_external($user_name, $remote_user_id, $user_guid)
    {
        if (!isset($user_name) || !isset($remote_user_id)) return false;

        try {
            $newUserKey = $this->generateNewHash();

            $stmt = $this->db->prepare("INSERT INTO users(user_name, user_guid, user_key, remote_user_id, user_role, date_registered, has_full_device_access)
                                               VALUES(:uname, :uguid, :ukey, :uremoteid, 0, NOW(), 0)");

            $stmt->bindparam(":uname", $user_name);
            $stmt->bindparam(":uguid", $user_guid);
            $stmt->bindparam(":ukey", $newUserKey);
            $stmt->bindparam(":uremoteid", $remote_user_id);

            if ($stmt->execute()) {
                return array(
                    'userGuid' => $user_guid,
                    'userKey' => $newUserKey
                );
            } else return false;

        } catch (PDOException $e) {
            //echo $e->getMessage();
            return false;
        }
    }

    public function bind_external_to_admin($remoteUserId, $accessKey, $userGuid)
    {
        if (!isset($remoteUserId) || !isset($accessKey)) return false;

        $adminId = $this->check_for_admin_key($accessKey);

        if ($adminId) {
            $currentGuid = $this->get_guid_for_user($adminId);

            if ($currentGuid) $userGuid = $currentGuid;

            try {
                $stmt = $this->db->prepare("UPDATE users
                    SET user_guid = :uguid, remote_user_id = :uremoteid
                    WHERE user_id = :uid");

                $stmt->bindparam(":uid", $adminId);
                $stmt->bindparam(":uguid", $userGuid);
                $stmt->bindparam(":uremoteid", $remoteUserId);

                if ($stmt->execute()) {
                    return array(
                        'userGuid' => $userGuid
                    );
                } else return false;

            } catch (PDOException $e) {
                //echo $e->getMessage();
                return false;
            }
        }
    }

    public function check_for_admin_key($key)
    {
        if (!isset($key)) {
            return false;
        }
        try {
            $stmt = $this->db->prepare("SELECT * FROM users WHERE user_key = :ukey LIMIT 1");
            $success = $stmt->execute(array(':ukey' => $key));

            if ($success) {
                $userRow = $stmt->fetch(PDO::FETCH_ASSOC);

                if ($userRow && $stmt->rowCount() == 1) {
                    $isValidAdminKey = ($userRow['user_role'] == 1);

                    if ($isValidAdminKey) {
                        return $userRow['user_id'];
                    }
                }

                return false;
            }

            return false;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }


    public function get_guid_for_user($userId)
    {
        if (!isset($key)) {
            return false;
        }
        try {
            $stmt = $this->db->prepare("SELECT * FROM users WHERE user_id = :uid LIMIT 1");
            $success = $stmt->execute(array(':uid' => $userId));

            if ($success) {
                $userRow = $stmt->fetch(PDO::FETCH_ASSOC);

                if ($userRow && $stmt->rowCount() == 1) {
                    return $userRow['user_guid'];
                }

                return false;
            }

            return false;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    private function generateNewHash()
    {
        return hash('md5', time());
    }
}
